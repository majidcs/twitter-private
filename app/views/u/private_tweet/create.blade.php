@extends('u.index')
@section('title')
	Private Tweets | New Private Tweets
@stop
@section('main_content')
	@include('u.layouts._top')
	@include('u.private_tweet._form')
@stop
@section('specific_scripts')
@stop
