@extends('u.index')
@section('title')
  Private Tweets | Private Tweets not found
@stop
@section('main_content')  
<!-- MAIN SECTION -->
@include('u.layouts._top')
<section class ="main text-center">
  <div class="container responsive">
    <div class="clearfix"><br/><br/></div>
   
    <div class="row">
      <label>This message was not found or probably deleted</label>
    </div>
    <div class="clearfix"><br/><br/></div> 
  </div>
</section>
@include('u.layouts._footer')

@stop
@section('specific_scripts')
@stop
