<!-- HEADER SECTION -->
<section class ="header-wrapper">
	<div class="row">
		<div class="col-md-12">
			@if(Session::has('access_token'))
				@if(isset(UserHelper::current()->t_avatar))
					<img src="{{UserHelper::current()->t_avatar}}" class="top-left-avatar"></img>
				@endif
				<a href="/logout" title="Logout" class="pull-right top-logout"><i class="fa fa-sign-out" style="color:#6AC3EB;font-size: 38px;">&nbsp;&nbsp;</i></a>
				@if(isset(UserHelper::current()->t_screen_name) /*&& UserHelper::current()->t_screen_name=='@MajidCS'*/)
					<a href="/admin" title="Admin" class="pull-right"><i class="fa fa-bar-chart-o" style="color:#6AC3EB;font-size: 38px;">&nbsp;&nbsp;</i></a>
				@endif
			@else
				<br/>
			@endif
			<a style="padding:0px 30px;margin-right: -16px;" href="/" title="Home" class="pull-right"><i class="fa fa-home" style="color:#6AC3EB;font-size: 38px;">&nbsp;&nbsp;</i></a>
		</div>
	</div>
</section> 
<!-- MAIN SECTION -->
<section class ="main text-center">
  <div class="container responsive">
    <div class="row logo">
      <img src="assets/logo.png" alt="Private Tweets"><h1>Private Tweets</h1>
    </div>

    <!-- Text area here -->
  </div>
</section>
