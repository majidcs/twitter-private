<div class="clearfix"><br/><br/></div>
<div class="container responsive text-center">
 <div class="row">
@if(Session::has('access_token'))
  <a href="u/private-tweet/create" class="btn btn-lg btn-twitter-login" ><i class="fa fa-twitter">&nbsp;</i>New Private Tweet</a>
@else
  <a href="/twitter-redirect" class="btn btn-lg btn-twitter-login" ><i class="fa fa-twitter">&nbsp;</i>Log In With Twitter</a>
@endif
</div>
<div class="clearfix"><br/><br/></div>
<div class="row">
  <p>
    <a href="/privacy" title="Privacy Policy">Privacy Policy</a>
  </p>
</div>
</div>