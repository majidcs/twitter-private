<!-- MAIN SECTION -->
<section class ="main text-center">
  <div class="container responsive">
    <div class="row logo">
      <img src="assets/logo.png" alt="Private Tweets"><h1>Private Tweets</h1>
    </div>

    <div class="row">
      <label>This message is private for this account</label>
    </div>
    
    <div class="row">
      <div class="inline">
        <img class="avatar" src="assets/sample-avatar.png" alt="@Username">
      </div>
      <div class="inline">
        <label>@Username</label>
      </div>
    </div>
    <div class="clearfix"><br/><br/></div>

    <div class="row">
      <button class="btn btn-lg btn-twitter-login" ><i class="fa fa-twitter">&nbsp;</i>Log In With Twitter</button>
    </div>
    <div class="clearfix"><br/><br/></div>
    <div class="row">
      <p>
        <a href="/privacy" title="Privacy Policy">Privacy Policy</a>
      </p>
    </div>
  </div>
</section>
