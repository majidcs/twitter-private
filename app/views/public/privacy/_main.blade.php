<!-- MAIN SECTION -->
<section class ="main text-left">
  <div class="container responsive">
    <div class="row logo">
      <img src="assets/logo.png" alt="Private Tweets"><h1>Private Tweets</h1><br/><br/>
    </div>

    <div class="row text-center">
      <h2>Privacy Policy</h2>
    </div>
    
    <div class="row">
      <label>How secure is Private Tweets ?</label>
      <p class="cl-blue">We've followed Twitter's <a href="https://dev.twitter.com/docs/security/best-practices">best practices</a> for security to ensure that their API. That means that if you can trust Twitter to send something, you can trust us.</p>
    <p class="cl-blue">If there are any questions you can contact us support@pvttwt.com</p>
    </div>
    <div class="clearfix"><br/><br/></div>
    <div class="row">
      <label>Why do we request the permissions that we do ?</label>
      <p class="cl-blue">We ask for the minimum amount of permissions we can from Twitter that enables Private Tweets to post your tweet.</p>
    </div>
    <div class="clearfix"><br/><br/></div>
    <div class="row">
      <label>What we'll do with that permission ?</label>
      <p class="cl-blue">We won't update your bio, photos, links, etc.&nbsp;We will only post the tweets you tell us to.</p>
    </div>
    <div class="clearfix"><br/><br/></div>
    <div class="row">
      <label>Information Collection and Use</label>
      <p class="cl-blue">We do not collect personally identifiable information about you , unless you give us such as email, In addition to that all information are handled strictly confidential, and it is not sold or shared with any third party.</p>
    </div>
    <div class="clearfix"><br/><br/></div>
    <div class="row">
      <label>Approval of the privacy statement.</label>
      <p class="cl-blue">Your use of the site, you agree to this Privacy Statement. We will update this statement from time to time  to reflect changes in our business,  so please visit privacy statement page constantly.</p>
    </div>
    <div class="clearfix"><br/><br/></div>
    <div class="row text-center">
      @if(Session::has('access_token'))
      <a href="u/private-tweet/create" class="btn btn-lg btn-twitter-login" ><i class="fa fa-twitter">&nbsp;</i>New Private Tweet</a>
      @else
        <a href="/twitter-redirect" class="btn btn-lg btn-twitter-login" ><i class="fa fa-twitter">&nbsp;</i>Log In With Twitter</a>
      @endif
    </div>
  </div>
</section>
