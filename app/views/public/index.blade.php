@extends('base')
@section('head')
	@section('title')
		Private Tweets 
	@stop
	<link rel="icon" href="assets/logo.png" type="image/x-icon">
	{{stylesheet_link_tag('public')}}
@stop
@section('body')
	<div id="wrapper">
	@include('public.layouts._top_nav')
	@yield('main_content')
	@include('public.layouts._footer')
	</div>
@stop
@section('scripts')
	{{javascript_include_tag('public')}}
	@yield('specific_scripts')
@stop
