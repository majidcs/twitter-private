 <!DOCTYPE html>
<html> 
<head>
	<title>
	@yield('title')
	</title>
	@yield('meta')
	<!-- Meta -->
	<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<link rel="shortcut icon" href="assets/logo.png">
	<meta content="" name="description" />
	<meta content="" name="author" />
	@yield('head')
	<base href="{{URL::to('/')}}" />
</head>
<body @yield('body_attr')>
	@yield('body')
	@yield('scripts')
</body>
</html>
<!--
=====================================================
pvttwt.com

Developer: Majid ALmatiree
=====================================================
--> 
