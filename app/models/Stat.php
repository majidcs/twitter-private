<?php

use LaravelBook\Ardent\Ardent;
use Codesleeve\Stapler\ORM\StaplerableInterface;
use Codesleeve\Stapler\ORM\EloquentTrait;

class Stat extends Ardent implements StaplerableInterface {
    use EloquentTrait;

	protected $fillable = ['ip'];

	public static function users_count(){
		return User::all()->count();
	}
	public static function most_visited_month(){
		return DB::table('stats')->select(DB::raw('date_tracked'))->groupBy('date_tracked')->orderBy(DB::raw('COUNT(date_tracked)'), 'desc')->first();
	}
}