<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/
// public routes
Route::get('privacy', function(){
	return View::make('public.privacy.index');
});
Route::get('/twitter-redirect', ['as'=>'twitter_login','uses'=>'TwitterController@login']);
Route::get('/twitter-auth', ['as'=>'twitter_auth','uses'=>'TwitterController@auth']); 
Route::get('/logout',['as'=>'logout','uses'=>'TwitterController@logout']);
Route::resource('admin', 'AdminController');

Route::group(['prefix'=>'u','before'=>'twitter_auth'], function(){
	Route::get('/', ['as'=>'u_index', 'uses'=>'PrivateTweetController@index']);
	Route::resource('private-tweet', 'PrivateTweetController');
	Route::get('/search-user', function(){
		$q = Input::get('q');
		return UserHelper::search(['q'=>$q]);
	});
});

Route::get('/{slug}',['as'=>'view_message','uses'=>'PrivateTweetController@show']);

Route::get('/', ['as' => 'public_routes', 'uses'=>'PublicController@index']);
Route::get('/{path}', ['as' => 'public_routes', 'uses'=>'PublicController@index']);

//delete
Route::post('/del/{id}', ['as'=>'delete', 'uses'=>'PrivateTweetController@destroy']);

Tracker::run();
