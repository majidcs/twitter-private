<?php
class TwitterController extends BaseController {
	public function login()
	{
		// Reqest tokens
	    $tokens = Twitter::oAuthRequestToken();

	    // Redirect to twitter
	    Twitter::oAuthAuthenticate(array_get($tokens, 'oauth_token'));
	    exit;
	}
	public function auth(){
		// Oauth token
	    if(!Input::has('oauth_token'))
	    	return Redirect::to('/');

	    $token = Input::get('oauth_token');

	    // Verifier token
	    $verifier = Input::get('oauth_verifier');

	    // Request access token
	    $accessToken = Twitter::oAuthAccessToken($token, $verifier);
	    if(isset($accessToken['oauth_token'])){
			Session::put('access_token', $accessToken);
			$user = UserHelper::save();
		    return Redirection::afterLogin();
		}else{
			return Redirect::to('/');
		}
	}
	public function logout(){
		Session::forget('access_token');
		return Redirection::afterLogout();
	} 
}
