<?php
class PublicController extends BaseController {
	public function index($path='home')
	{	
		if(Session::has('access_token'))
			return View::make('u.private_tweet.create');
		
		$viewPath = 'public.'.$path.'.index';
		if(View::exists($viewPath))
			return View::make($viewPath);
		else
			return Response::view('errors.404', array(), 404);
	}
}
