<?php

class AdminController extends BaseController {

	public function index()
	{
		if(!Session::has('access_token'))
			return Redirect::to('/');

		// if(UserHelper::current()->t_screen_name!='@MajidCS')
		// 	return Redirect::to('/');

		return View::make('u.admin.index');
	}

}
