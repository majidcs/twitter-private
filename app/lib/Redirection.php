<?php
class Redirection{
	public static function afterLogin(){
		if (Session::has('intented_route')){
			$r = Session::get('intented_route');
			Session::forget('intented_route');		
			return Redirect::away($r);
		}else
			return Redirect::to('/u');
	}

	public static function afterLogout(){
		return Redirect::to('/');
	}

	public static function reqNotPermitted(){
		return Response::json(['status'=>'fail','message'=>'Unauthorized'], 401);
	}

	public static function unknownError(){
		return Response::json(['status'=>'fail', 'message'=>'Unknown Error Occur! Please try again.'], 500);
	}
}