<?php
class UrlHelper{
	public static function shorten($url=null){
		return Bitly::shorten($url)->getResponseData()['data']['url'];
	} 
}