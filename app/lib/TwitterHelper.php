<?php
class TweeterHelper{
	public static function tweet($params=[]){
		$url = "https://twitter.com/intent/tweet?via=TweetPrivate&text=".urlencode($params['content'])."&url=".$params['url']."&original_referer".URL::to('/u/private-tweet/create');
		// Header('Location: '.$url);
		return Redirect::away($url);
	}
}