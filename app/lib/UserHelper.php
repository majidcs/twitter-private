<?php
class UserHelper{
	public static function save(){
		$oAuth = Session::get('access_token');
		if(!isset($oAuth['oauth_token']))
			return Redirect::to('/');

		$twitter = Twitter::Usersshow($oAuth['user_id']);

		$user = User::where('t_id', $oAuth['user_id'])->orWhere('t_screen_name', '@'.trim($twitter['screen_name']))->first();
		$user = $user ? $user : new User();
		$user->t_id = trim($oAuth['user_id']);
		$user->name = $twitter['name'];
		$user->t_screen_name = '@'.trim($twitter['screen_name']);
		$user->t_avatar = $twitter['profile_image_url_https'];
		$user->t_location = $twitter['location'];
		$user->t_description = $twitter['description'];
		$user->save();
	}
	public static function current(){
		if(!Session::has('access_token'))
			return Redirect::to('u/private-tweet/create');
		
		$twitter = Session::get('access_token');
		
		$user = User::where('t_id', $twitter['user_id'])->orWhere('t_screen_name', '@'.trim($twitter['screen_name']))->first();

		if($user){
			return $user;
		}else{
			UserHelper::save();
			return User::where('t_id', $twitter['user_id'])->orWhere('t_screen_name', '@'.trim($twitter['screen_name']))->first();
		}
	
	}
	public static function currentFresh(){
		$oAuth = Session::get('access_token');

		$twitter = Twitter::Usersshow($oAuth['user_id']);
		return $twitter;

	}
	public static function getUser($params=[]){
		$params['id'] = isset($params['id']) ? $params['id'] : null;
		$params['screen_name'] = isset($params['screen_name']) ? $params['screen_name'] : null;
		$params['screen_name'] = str_replace('@', '', $params['screen_name']);
		return Twitter::usersShow($params['id'], $params['screen_name']);	
	}
	public static function search($q=''){
		$search = TwitterApi::getUsersSearch(['q'=>$q]);
		$objArr = [];
		if(is_array($search)){
			foreach($search as $key => $result) {
				if(isset($result->screen_name)){
					$screen_name = $result->screen_name;
					$item = ['id'=>$screen_name,'name'=>$screen_name];
					$objArr[] = $item;
				}
			}
		}
		return $objArr;
	}
}