<?php
class Tracker{
	public static function run(){
		$end_ip = self::client_ip();// client_ip
		$today = date('Y-m-d');//date now

		$check_ip = Stat::where('ip', $end_ip)->where('date_tracked', $today)->first();

		if(!$check_ip){
			$stat = new Stat();
			$stat->ip = $end_ip;
			$stat->date_tracked = $today;
			$stat->save();
		}
	}

	public static function client_ip(){
		if(!isset($_SERVER['HTTP_CLIENT_IP']) && !isset($_SERVER['HTTP_X_FORWARDED_FOR']) && !isset($_SERVER['REMOTE_ADDR']))
			return false;

		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
		    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
		    $ip = $_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
}