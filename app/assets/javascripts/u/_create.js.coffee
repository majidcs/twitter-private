$(document).ready   ->   
	$("#summernote").summernote
        placehoder: "Private message here..."
        height: 200
        minHeight: null
        maxHeight: null
        maxWidth: 750
        focus: false
        toolbar:	[
        	[
                "style"
                [
                    "bold"
                    "italic"
                    "underline"
                    "clear"
                ]
            ]
            ['color'
                ['color']
            ]
        	[
        		"para"
        		[
        			"ul"
        			"ol"
        			"paragraph"
        		]
        	]
            [
                "insert"
                [
                    "video"
                    "link"
                    "picture"
                ]
            ]
        ]
    $('input[type=file]').bootstrapFileInput()
    

$('.form-alerts').delay(5000).fadeOut(400)
$('.form-alerts').click ()->
	$(this).remove()
 
query = (term, callback)->
    $.get($('base').attr('href')+'/u/search-user?q='+term).success (res)->
        callback(res)